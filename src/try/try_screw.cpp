#include <iostream>
#include <rw/rw.hpp>


using namespace std;
USE_ROBWORK_NAMESPACE
using namespace robwork;


int main(int argc, char* argv[]) {
  
  Transform3D<> t1(Vector3D<>(0.0, 0.0, 0.0));
  Transform3D<> t2(Vector3D<>(0.0, 0.0, 0.1));
  
  Transform3D<> dt = t2 * t1;
  VelocityScrew6D<> vs(dt);
  
  cout << vs << endl;
  
  return 0;
}
