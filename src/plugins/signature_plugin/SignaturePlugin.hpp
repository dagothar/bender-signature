#pragma once

#include <QObject>
#include <rws/RobWorkStudioPlugin.hpp>
#include <rw/models/WorkCell.hpp>
#include <QtGui>

#include "ui_SignaturePlugin.h"

/**
 * @brief A plugin for testing grippers.
 */
class SignaturePlugin: public rws::RobWorkStudioPlugin {
	Q_OBJECT
	Q_INTERFACES(rws::RobWorkStudioPlugin)

public:
	//! @brief constructor
	SignaturePlugin();

	//! @brief destructor
	virtual ~SignaturePlugin();

	//! @copydoc rws::RobWorkStudioPlugin::open(rw::models::WorkCell* workcell)
	virtual void open(rw::models::WorkCell* workcell);

	//! @copydoc rws::RobWorkStudioPlugin::close()
	virtual void close();

	//! @copydoc rws::RobWorkStudioPlugin::initialize()
	virtual void initialize();

	/**
	 * @brief listen for generic event
	 * @param event [in] the event id
	 */
	void genericEventListener(const std::string& event);

private slots:
	
  void loadEvent();
  void relativeEvent();
  void planEvent();
  void saveEvent();
  
  void drawEvent();

private:
  /* plugin interface widget */
	Ui::SignaturePluginWidget _ui;
  
  /* essentials */
  rw::models::WorkCell* _wc;
  rw::kinematics::State _initState;
  
  /* robot and trajectory */
  
};
