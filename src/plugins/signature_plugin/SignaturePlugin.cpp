#include "SignaturePlugin.hpp"
#include <rws/RobWorkStudio.hpp>
#include <boost/bind.hpp>

using namespace std;
using namespace rws;
using namespace rw::common;
using namespace rw::math;
using namespace rw::models;
using namespace rw::kinematics;
using namespace boost;

SignaturePlugin::SignaturePlugin() :
  RobWorkStudioPlugin("SignaturePlugin", QIcon(":/pa_icon.png")) {
  _ui.setupUi(this);
  
  /* ... */
}

SignaturePlugin::~SignaturePlugin() {
}

void SignaturePlugin::initialize() {
  
  getRobWorkStudio()->genericEvent().add(boost::bind(&SignaturePlugin::genericEventListener, this, _1), this);
}

void SignaturePlugin::open(WorkCell* workcell) {
  
  try {
      _wc = workcell;
      _initState = getRobWorkStudio()->getState();
      
      /* ... */
      
  } catch (const rw::common::Exception& e) {
      QMessageBox::critical(NULL, "RW Exception", e.what());
  }
}

void SignaturePlugin::close() {
  /* ... */
}

void SignaturePlugin::genericEventListener(const std::string & event) {

  /*if (event == "DynamicWorkCellLoaded") {
    
  }*/
}


void SignaturePlugin::loadEvent() {
  QString filename = QFileDialog::getOpenFileName(this, "Open file", "", tr("Trajectory files (*.csv)"));

  if (filename.isEmpty()) {
      return;
  }

  log().info() << "Loading trajectory from: " << filename.toStdString() << "\n";

  StandardGripperLoader::Ptr loader = new StandardGripperLoader();
  _gripper = loader->load(filename.toStdString()).cast<StandardGripper>();

}


void SignaturePlugin::relativeEvent() {
}


void SignaturePlugin::planEvent() {
}


void SignaturePlugin::saveEvent() {
}


void SignaturePlugin::drawEvent() {
}

Q_EXPORT_PLUGIN(SignaturePlugin);
