#include <iostream>
#include <cstdlib>
#include <rw/rw.hpp>
#include <string>
#include <fstream>
#include <exception>
#include <sstream>
#include <rw/trajectory/Timed.hpp>
#include <rw/trajectory.hpp>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <rw/trajectory/CubicSplineFactory.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>


using namespace std;
using namespace boost;
USE_ROBWORK_NAMESPACE
using namespace robwork;


TimedQPath load_data(const string& filename) {
  TimedQPath timed_path = TimedQPath();
  
  ifstream in(filename.c_str());
  if (!in.is_open()) {
    throw new runtime_error("Could not open file!");
  }
  
  string line;
  while(getline(in, line)) {
    vector<string> tokens;
    
    typedef tokenizer<escaped_list_separator<char> > Tokenizer;
    Tokenizer tokenizer(line);
    tokens.assign(tokenizer.begin(), tokenizer.end());
    
    double x = stod(tokens[0]);
    double y = stod(tokens[1]);
    double t = stod(tokens[2]);
    
    timed_path.push_back(TimedQ(t, Q(2, x, y)));
  }
  
  return timed_path;
}


typedef Timed<Transform3D<> > TimedTransform3D;
typedef Path<TimedTransform3D> TimedTransform3DPath;


TimedTransform3DPath make_transform_path(const TimedQPath& path, const Transform3D<>& transform) {
  TimedTransform3DPath tpath;
  
  BOOST_FOREACH (TimedQ tq, path) {
    Transform3D<> t = transform;
    t.P()[0] += tq.getValue()[0];
    t.P()[1] += tq.getValue()[1];
    
    tpath.push_back(TimedTransform3D(tq.getTime(), t));
  }
  
  return tpath;
}


template <class T>
ostream& operator<<(ostream& stream, const Path<Timed<T> >& tpath) {
  BOOST_FOREACH (Timed<T> tq, tpath) {
    stream << tq.getTime() << ": " << tq.getValue() << endl;
  }
  
  return stream;
}


TimedQPath make_joint_path(const TimedTransform3DPath& tpath, SerialDevice::Ptr device, const State& state) {
  TimedQPath jpath;
  
  InvKinSolver::Ptr solver = new ClosedFormIKSolverUR(device, state);
  
  Q prev_solution = device->getQ(state);
  BOOST_FOREACH (TimedTransform3D t, tpath) {
    vector<Q> solutions = solver->solve(t.getValue(), state);
    
    cout << "Found " << solutions.size() << " solutions..." << endl;
    
    // pick closest solution
    double max_dist = 1000.0;
    Q solution = solutions[0];
    for (vector<Q>::iterator i = solutions.begin(); i != solutions.end(); ++i) {
      double distance = (prev_solution - *i).norm2();
      if (distance < max_dist) {
        max_dist = distance;
        solution = *i;
      }
    }
    
    prev_solution = solution;
    
    jpath.push_back(TimedQ(t.getTime(), solution));
  }
  
  return jpath;
}


int main(int argc, char* argv[]) {
  Q initial = Q(6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (argc == 7) {
    initial[0] = atof(argv[1]);
    initial[1] = atof(argv[2]);
    initial[2] = atof(argv[3]);
    initial[3] = atof(argv[4]);
    initial[4] = atof(argv[5]);
    initial[5] = atof(argv[6]);
  }
  cout << "Initial robot configuration: " << initial << endl;
  
  // load workcell
  WorkCellLoader::Ptr loader = new XMLRWLoader();
  WorkCell::Ptr wc = loader->loadWorkCell("../bender_wc/Scene.wc.xml");
  if (!wc) {
    cout << "Could not load workcell!" << endl;
    return -1;
  }
  cout << "Loaded workcell: " << wc->getName() << endl;
  State state = wc->getDefaultState();
  
  // find UR
  SerialDevice::Ptr ur = wc->findDevice<SerialDevice>("UR1");
  if (!ur) {
    cout << "Could not find the robot device!" << endl;
    return -1;
  }
  
  // find initial ee pose
  ur->setQ(initial, state);  
  Transform3D<> initial_pose = ur->baseTend(state);
  cout << "Initial pose: " << initial_pose << endl;
  
  // load xy data
  TimedQPath timed_path = load_data("../data/data1.csv");
  cout << "Loaded timed path: " << timed_path << endl;
  
  // make data relative to initial robot pose
  TimedTransform3DPath transform_path = make_transform_path(timed_path, initial_pose);
  cout << "Transform path: " << transform_path << endl;
  
  // generate ik solutions
  TimedQPath joint_path = make_joint_path(transform_path, ur, state);
  cout << "Joint path: " << joint_path << endl;
  
  return 0;
}
