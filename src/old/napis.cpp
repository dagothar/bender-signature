#include <iostream>
#include <string>
#include <fstream>
#include <exception>
#include <sstream>
#include <rw/trajectory/Timed.hpp>
#include <rw/trajectory.hpp>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <rw/trajectory/CubicSplineFactory.hpp>


using namespace std;
USE_ROBWORK_NAMESPACE
using namespace robwork;
using namespace boost;


TimedQPath load_data(const string& filename) {
  TimedQPath timed_path = TimedQPath();
  
  ifstream in(filename.c_str());
  if (!in.is_open()) {
    throw new runtime_error("Could not open file!");
  }
  
  string line;
  while(getline(in, line)) {
    vector<string> tokens;
    
    typedef tokenizer<escaped_list_separator<char> > Tokenizer;
    Tokenizer tokenizer(line);
    tokens.assign(tokenizer.begin(), tokenizer.end());
    
    double x = stod(tokens[0]);
    double y = stod(tokens[1]);
    double t = stod(tokens[2]);
    
    timed_path.push_back(TimedQ(t, Q(2, x, y)));
  }
  
  return timed_path;
}


int main(int argc, char* argv[]) {
  
  TimedQPath timed_path = load_data("../data/data1.csv");
  
  InterpolatorTrajectory<rw::math::Q>::Ptr it = CubicSplineFactory::makeNaturalSpline(&timed_path);
  
  double duration = it->duration();
  //cout << "Computed spline: duration=" << duration << " segments=" << it->getSegmentsCount() << endl;
  
  for (double t = 0.0; t < duration; t += 0.1) {
    Q q = it->x(t);
    Q dq = it->dx(t);
    cout << t << ", " << q[0] << ", " << q[1] << ", " << dq[0] << ", " << dq[1] << endl;
  }
  
  return 0;
}
