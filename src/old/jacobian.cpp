#include <iostream>
#include <rw/rw.hpp>


using namespace std;
USE_ROBWORK_NAMESPACE
using namespace robwork;


int main(int argc, char* argv[]) {
  // load workcell
  WorkCellLoader::Ptr loader = new XMLRWLoader();
  WorkCell::Ptr wc = loader->loadWorkCell("../bender_wc/Scene.wc.xml");
  cout << "Loaded workcell: " << wc->getName() << endl;
  State state = wc->getDefaultState();
  
  // find UR
  Device::Ptr ur1 = wc->findDevice("UR1");
  Jacobian jacobian = ur1->baseJend(state);
  cout << "Jacobian: " << jacobian << endl;
  
  return 0;
}
